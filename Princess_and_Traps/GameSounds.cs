﻿using System;

namespace Princess_and_Traps
{
    static class GameSounds
    {
        public static void PlayDefault(int length)
        {
            Console.Beep(250, length);
        }

        public static void PlayTraped()
        {
            Console.Beep(550, 150);
            Console.Beep(750, 150);
            Console.Beep(550, 150);
        }

        public static void PlayDeath()
        {
            Console.Beep(250, 150);
            Console.Beep(350, 150);   
        }

        public static void PlayWin()
        {
            Console.Beep(750, 150);
            Console.Beep(650, 150);
            Console.Beep(550, 150);
            Console.Beep(750, 150);
        }

    }
}
