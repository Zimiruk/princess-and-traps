﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Princess_and_Traps
{
    public class Level
    {
        public bool Completed { get; set;} = false;
        public bool Failed { get; set; } = false;

        private int levelSize_x;
        private int levelSize_y;

        private int animationFlag = 0;

        private Character _hero;
        private Character _princess;

        private List<Trap> traps = new List<Trap>();

        public (int, int) heroLastCoordinate;

        public Level(Character hero, Character princess, int size_x, int size_y)
        {
            _hero = hero;
            _princess = princess;

            levelSize_x = size_x;
            levelSize_y = size_y;

            FillLevelWithTraps();
        }

        private void FillLevelWithTraps()
        {
            for (int i = 0; traps.Count < 10; i++)
            {
                Trap trap = new Trap();

                if (trap.Coordinate_x == _princess.Coordinate_x && trap.Coordinate_y == _princess.Coordinate_y)
                    continue;

                if (trap.Coordinate_x == _hero.Coordinate_x && trap.Coordinate_y == _hero.Coordinate_y)
                    continue;

                int index = traps.FindIndex(found => found.Coordinate_x == trap.Coordinate_x && found.Coordinate_y == trap.Coordinate_y);
                if (index == -1)
                {
                    traps.Add(trap);
                }
            }
        }

        private bool CheckForFound()
        {
            if (_hero.Coordinate_x == _princess.Coordinate_x && _hero.Coordinate_y == _princess.Coordinate_y)
                return true;
            return false;
        }

        private void CheckForTrapped()
        {
            int index = -1;
            index = traps.FindIndex(trap => trap.Coordinate_x == _hero.Coordinate_x && trap.Coordinate_y == _hero.Coordinate_y);

            if (index >= 0)
            {
                if (!_hero.Traped)
                {
                    _hero.Health -= traps[index].TrapDamage;
                    _hero.Traped = true;
                    Task.Run(() => GameSounds.PlayTraped());           
                }
            }
            else
                _hero.Traped = false;
        }

        private bool CheckForDeath()
        {
            if (_hero.Health <= 0)
                return true;
            return false;
        }

        private void CheckForBorder()
        {
            if (!(0 <= _hero.Coordinate_x && _hero.Coordinate_x <= levelSize_x - 1) || !(0 <= _hero.Coordinate_y && _hero.Coordinate_y <= levelSize_y - 1))
            {
                _hero.Coordinate_x = heroLastCoordinate.Item1;
                _hero.Coordinate_y = heroLastCoordinate.Item2;
            }
        }
        private void ChangeanimationFlag(ref int animationFlag, CancellationToken gameCancelToken)
        {
            while (!gameCancelToken.IsCancellationRequested)
            {
                Thread.Sleep(1000);
                animationFlag = animationFlag == 0 ? 1 : 0;
            }
        }

        private void DrawLevel()
        {
            Console.SetCursorPosition(0, 0);

            Console.ForegroundColor = ConsoleColor.Green;
            for (int y = 0; y < levelSize_y; y++)
            {
                for (int x = 0; x < levelSize_x; x++)
                {
                    string symbol;

                    if (_hero.Coordinate_x == x && _hero.Coordinate_y == y)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        symbol = _hero.Symbol;
                    }

                    else if (_princess.Coordinate_x == x && _princess.Coordinate_y == y)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        symbol = _princess.Symbol;
                    }

                    else
                    {
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            symbol = animationFlag == 0 ? "`" : "'";
                        }
                    }

                    Console.Write(symbol);
                }
                Console.WriteLine();
            }

            Console.ForegroundColor = ConsoleColor.White;
            Common.ClearCurrentConsoleLine();
            Console.WriteLine("Character hp = " + _hero.Health);
            if(_hero.Traped)
            {
                Common.ClearCurrentConsoleLine();
                Console.WriteLine($"{_hero.Symbol} caught in trap");
            }     
            else
            {
                Common.ClearCurrentConsoleLine();
                Console.WriteLine("");
            }
        }

        public void CollectLevelInfo(CancellationToken gameCancelToken, CancellationTokenSource gameCancelSource)
        {
            Task.Run(() => ChangeanimationFlag(ref animationFlag, gameCancelToken));

            while (!gameCancelToken.IsCancellationRequested)
            {
                if (CheckForFound())
                {
                    Completed = true;
                    Task gametask = Task.Run(() => GameSounds.PlayWin());

                    DrawLevel();
                    gametask.Wait();      
                    
                    break;
                }
                else if (CheckForDeath())
                {
                    Failed = true;
                    Task gametask = Task.Run(() => GameSounds.PlayDeath());
                    
                    DrawLevel();
                    gametask.Wait();

                    break;
                }

                DrawLevel();

                CheckForBorder();
                CheckForTrapped();

                Thread.Sleep(50);
            }

            gameCancelSource.Cancel();
        }
    }
}
