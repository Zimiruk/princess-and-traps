﻿using Princess_and_Traps.Game_objects;

namespace Princess_and_Traps
{
    internal class Program
    {
        static void Main(string[] args)
        {
            bool ExitGame = false;

            while (!ExitGame)
            {
                Gameplay gameplay = new Gameplay();
                gameplay.DisplayContent();

                GameComplition gameComplition = new GameComplition();
                gameComplition.DisplayContent();

                if (gameComplition.ExitGame)
                    ExitGame = true;

            }
        }
    }
}
