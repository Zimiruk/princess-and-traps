﻿using System;

namespace Princess_and_Traps
{
    class Trap : GameObject
    {
        public int TrapDamage { get; }

        public Trap()
        {
            Random randNum = new Random();

            TrapDamage = randNum.Next(1, 10);
            Coordinate_x = randNum.Next(1, 9);
            Coordinate_y = randNum.Next(1, 9);
        }
    }
}
