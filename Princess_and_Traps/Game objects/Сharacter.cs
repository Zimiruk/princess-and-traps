﻿using System;
using System.Threading.Tasks;

namespace Princess_and_Traps
{
    public class Character : GameObject
    {
        public int Health { get; set; } = 10;
        public bool Traped { get; set; }
        public string Symbol { get; }

        public Character(int x, int y, string symbol)
        {
            Coordinate_x = x;
            Coordinate_y = y;
            Symbol = symbol;
        }

        public void Move(ConsoleKeyInfo movementKey)
        {
            Task.Run(() => GameSounds.PlayDefault(200));
            switch (movementKey.Key)
            {
                case ConsoleKey.UpArrow:
                    --Coordinate_y;
                    break;

                case ConsoleKey.DownArrow:
                    ++Coordinate_y;
                    break;

                case ConsoleKey.LeftArrow:
                    --Coordinate_x;
                    break;

                case ConsoleKey.RightArrow:
                    ++Coordinate_x;
                    break;
            }            
        }
    }
}
