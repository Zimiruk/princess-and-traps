﻿using System;
using System.Threading;

namespace Princess_and_Traps.Stages
{
    public abstract class GameStage
    {
        public abstract void DisplayContent();
        protected abstract void InputReader(CancellationTokenSource gameCancelSource);
        protected abstract void InputHandler(ConsoleKeyInfo inputKey);

    }
}
