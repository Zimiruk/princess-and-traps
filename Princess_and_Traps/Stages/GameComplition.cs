﻿using Princess_and_Traps.Stages;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Princess_and_Traps.Game_objects
{
    class GameComplition : GameStage
    {
        public bool ExitGame { get; private set; }
        private bool stopped;
        public override void DisplayContent()
        {
            CancellationTokenSource gameCancelSource = new CancellationTokenSource();
            CancellationToken gameCancelToken = gameCancelSource.Token;

            Console.WriteLine("Wanna play again Y/N ?");

            Task input = Task.Run(() => InputReader(gameCancelSource));

            input.Wait();
            Console.Clear();
        }

        protected override void InputReader(CancellationTokenSource gameCancelSource)
        {
            ConsoleKeyInfo inputKey;
            do
            {
                while (Console.KeyAvailable)
                {
                    Console.ReadKey(true);
                }
                inputKey = Console.ReadKey(true);

                InputHandler(inputKey);
                Thread.Sleep(100);
            }

            while (!stopped);
            gameCancelSource.Cancel();
        }

        protected override void InputHandler(ConsoleKeyInfo inputKey)
        {
            if (inputKey.Key == ConsoleKey.Y)
            {
                stopped = true;
            }
            else if (inputKey.Key == ConsoleKey.N)
            {
                ExitGame = true;
                stopped = true;
            }
        }
    }
}

