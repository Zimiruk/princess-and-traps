﻿using Princess_and_Traps.Stages;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Princess_and_Traps
{
    public class Gameplay : GameStage
    {
        private Character hero;
        private Character princess;
        private Level level;

        private bool messageDisplayed = false;
        private bool finished = false;

        public Gameplay()
        {
            Random randNum = new Random();

            hero = new Character(randNum.Next(0, 9), 9, "☺");
            princess = new Character(randNum.Next(0, 9), 0, "P");
            level = new Level(hero, princess, 10, 10);
        }

        public override void DisplayContent()
        {
            CancellationTokenSource gameCancelSource = new CancellationTokenSource();
            CancellationToken gameCancelToken = gameCancelSource.Token;

            Task gametask = Task.Run(() => level.CollectLevelInfo(gameCancelToken, gameCancelSource));
            Task input = Task.Run(() => InputReader(gameCancelSource));

            gametask.Wait();

            if (!finished)
            {
                if (level.Completed)
                {
                    DisplayMessage($"Thank You {hero.Symbol}, But Our Princess is in Another Room 10x10 with Traps");
                }

                else if (level.Failed)
                {
                    DisplayMessage("Unfortunately you are dead");
                }
            }

            finished = true;
            input.Wait();

            Console.Clear();
            Thread.Sleep(1000);
        }

        private void DisplayMessage(string message)
        {
            for (int i = 0; i < message.Length; i++)
            {
                if (messageDisplayed)
                {
                    Common.ClearCurrentConsoleLine();
                    Console.WriteLine(message);              
                    break;
                }

                GameSounds.PlayDefault(50);
                Console.Write(message[i]);
                Thread.Sleep(50);
            }
            Console.WriteLine();
            Console.WriteLine("Press any key");
        }

        protected override void InputReader(CancellationTokenSource gameCancelSource)
        {
            ConsoleKeyInfo inputKey;
            do
            {  
                while (Console.KeyAvailable)
                {
                    Console.ReadKey(true);
                }
                inputKey = Console.ReadKey(true);

                InputHandler(inputKey);
                Thread.Sleep(100);
            }

            while (!finished);
            gameCancelSource.Cancel();
        }

        protected override void InputHandler(ConsoleKeyInfo inputKey)
        {
            if (level.Completed || level.Failed)
            {
                if (inputKey.Key != 0)
                {
                    messageDisplayed = true;
                }
            }
            else
            {
                if (inputKey.Key == ConsoleKey.Escape)
                {
                    finished = true;
                }
                else
                {
                    level.heroLastCoordinate = (hero.Coordinate_x, hero.Coordinate_y);
                    hero.Move(inputKey);
                }
            }
        }
    }
}
